lcand n = [2..(n-1)]

temdiv n [] = False
temdiv n (x:xs)
 | mod n x == 0 = True
 | otherwise = temdiv n xs

primo n = not (temdiv n (lcand n))

filtro f [] = []
filtro f (x:xs)
 | f x = x : filtro f xs
 | otherwise = filtro f xs