membro x [] = False
membro x (y:ys)
 | x == y = True
 | otherwise = membro x ys

frutas = ["pera","lima","pessego","laranja"]
funcao = (\lista -> (\elemento -> membro elemento lista))

ehFruta = funcao ["foda","mal","dada"]

mult = (\vezes-> (\n -> n * vezes))
dobro = (\numero -> numero *2)
div2 = (\numero -> numero/2)
