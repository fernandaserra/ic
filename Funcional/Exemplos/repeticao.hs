repeticao :: [Int] -> [Int]
repeticao [] = [] 
repeticao [x] = [x]
repeticao (x:xs)
 | x == head xs = repeticao(xs)
 | otherwise = x:repeticao(xs)