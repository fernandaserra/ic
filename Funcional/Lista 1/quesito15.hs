unicorr :: Int -> [Int] -> Bool
unicorr _ [] = False
unicorr n (x:xs)
 | (x == n) && (unicorr n xs == False) = True
 | (x == n) && (unicorr n xs == True) = False
 |otherwise = unicorr n xs 