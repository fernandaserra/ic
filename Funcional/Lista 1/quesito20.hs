repetido :: [Int] -> Bool
repetido [] = False
repetido [x] = False
repetido (x:xs)
 | x == head xs = True
 | otherwise = repetido(xs)