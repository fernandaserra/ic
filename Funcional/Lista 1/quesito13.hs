maiormenor :: [Int] -> [Int]
maiormenor [] = []
maiormenor [x] = [x]
maiormenor lista = maior(lista):menor(lista):[]


menor :: [Int] -> Int
menor [x] = x
menor (x:xs)
 | x < menor(xs) = x
 | otherwise = menor(xs) 

maior :: [Int] -> Int
maior [x] = x
maior (x:xs)
 | x > maior(xs) = x
 | otherwise = maior(xs)