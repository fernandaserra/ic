ultimo :: [Int] -> Int
ultimo [] = 0
ultimo [x] = x
ultimo (x:xs)
 | xs == [] = x
 | otherwise = ultimo(xs)