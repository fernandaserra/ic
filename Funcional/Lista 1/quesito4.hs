ordena :: Int -> Int -> Int -> (Int,Int,Int)
ordena a b c
 | a > b = ordena(b a c)
 | b > c = ordena(a c b)
 | a > c = ordena(c b a)
 | otherwise = (a,b,c)