concatena :: [Int] -> [Int] -> [Int]
concatena [] x = x
concatena x [] = x
concatena [] [] = []
concatena x y
 | tail x == [] = head x:y
 | otherwise = head x:(concatena (tail x) y)