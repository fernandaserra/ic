semonum :: [Int] -> Int -> [Int]
semonum [] _ = []
semonum (x:xs) y
 | x == y = semonum xs y
 | otherwise = [x] ++ semonum xs y