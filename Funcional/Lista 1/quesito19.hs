par :: [Int] -> [Int]
par [] = []
par (x:xs)
 | mod x 2 == 0 = x:par(xs)
 | otherwise = par(xs)

impar :: [Int] -> [Int]
impar [] = [] 
impar (x:xs)
 | mod x 2 /= 0 = x:impar(xs)
 | otherwise = impar(xs)

parimpar :: [Int] -> ([Int],[Int])
parimpar [] = ([],[])
parimpar lista = (par(lista),impar(lista))