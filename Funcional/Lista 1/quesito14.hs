produto :: [Int] -> Int
produto [] = 0
produto [x] = x
produto (x:xs) = x * produto(xs)