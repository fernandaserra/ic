multiplicacao :: Int -> Int -> Int
multiplicacao _ 0 = 0
multiplicacao x 1 = x
multiplicacao x y 
 | y > 0 = x + multiplicacao x (y-1)
 | otherwise = (-x) + multiplicacao x (y+1)