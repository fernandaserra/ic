div3 :: Int -> Bool
div3 x
 | mod x 3 == 0 = True
 | otherwise = False

todos f [x] = True
todos f (x:xs)
 | f x == False = False
 | otherwise = todos f xs
