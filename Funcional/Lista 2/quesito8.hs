prim f [x] = x
prim f (x:xs)
 | f x = x
 | otherwise = prim f xs
