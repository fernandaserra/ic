remove :: Int -> [Int] -> [Int]
remove x [] = []
remove x (y:ys)
 | x == y = ys
 | otherwise = y : remove x ys
