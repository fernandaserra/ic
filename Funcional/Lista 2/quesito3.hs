bolha :: [Int] -> [Int]
bolha [x] = [x]
bolha (x:y:xs)
 | x < y = x:bolha(y:xs)
 | otherwise = y : bolha(x:xs)

ordem :: [Int] -> Bool
ordem [x] = True
ordem (x:y:xs)
 | x <= y = ordem(y:xs)
 | otherwise = False

ordena :: [Int] -> [Int]
ordena l
 | ordem l == True = l
 | otherwise =  ordena(bolha l)
