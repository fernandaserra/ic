retiraPrim f [] = []
retiraPrim f (x:xs)
 | f x = retiraPrim f xs
 | otherwise = (x:xs)
