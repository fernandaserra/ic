tomaPrim f [] = []
tomaPrim f (x:xs)
 | f x = x : tomaPrim f xs
 | otherwise = []
