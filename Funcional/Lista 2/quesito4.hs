par :: Int -> Bool
par x
 | mod x 2 == 0 = True
 | otherwise = False

qualquer :: (Int -> Bool) -> [Int] -> Bool
qualquer f [] = False
qualquer f (x:xs)
 | f x = True
 | otherwise = qualquer f xs
