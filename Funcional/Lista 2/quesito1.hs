dobro :: Int -> Int
dobro x = 2 * x

mapInt :: (Int -> Int) -> [Int] -> [Int]
mapInt f [] = []
mapInt f (x:xs) = f(x) : mapInt f xs
