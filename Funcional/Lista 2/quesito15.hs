desinfileirar :: [Int] -> [Int]
desinfileirar [x] = []
desinfileirar (x:xs) = x : desinfileirar xs
