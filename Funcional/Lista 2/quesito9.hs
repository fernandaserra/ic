div9 :: Int -> Bool
div9 x
 | mod x 9 == 0 = True
 | otherwise = False

separa :: (Int -> Bool) -> Bool -> [Int] -> [Int]
separa f b [] = []
separa f b (x:xs)
 | f x == b = x : separa f b xs
 | otherwise = separa f b xs

divLista :: (Int -> Bool) -> [Int] -> ([Int],[Int])
divLista f [] = ([],[])
divLista f lista = (separa f True lista, separa f False lista) 
