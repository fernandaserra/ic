busca :: Int -> [Int] -> Int
busca x [] = 0
busca x (y:ys)
 | x == y = x
 | otherwise = busca x ys
