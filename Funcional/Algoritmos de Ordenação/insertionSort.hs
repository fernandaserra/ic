insertion :: [Int] -> [Int]
insertion [] = []
insertion [x] = [x]
insertion (x:xs) = ordena x (insertion xs)

ordena :: Int -> [Int] -> [Int]
ordena x [] = [x]
ordena x (y:ys)
 | x <= y = (x:y:ys)
 | otherwise = y : ordena x ys
