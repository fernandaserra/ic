bolha :: [Int] -> [Int]
bolha [x] = [x]
bolha (x:y:xs)
 | x < y = x:bolha(y:xs)
 | otherwise = y : bolha(x:xs)

ordem :: [Int] -> Bool
ordem [x] = True
ordem (x:y:xs)
 | x <= y = ordem(y:xs)
 | otherwise = False

bsort :: [Int] -> [Int]
bsort l
 | ordem l == True = l
 | otherwise =  bsort(bolha l)
