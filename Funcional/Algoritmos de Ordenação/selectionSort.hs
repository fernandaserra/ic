menor :: [Int] -> Int
menor [x] = x
menor (x:y:xs)
 | x < y = menor(x:xs)
 | otherwise = menor(y:xs)

remove :: Int -> [Int] -> [Int]
remove y [] = []
remove y (x:xs)
 | y == x = xs
 | otherwise = x : remove y xs

selection :: [Int] -> [Int]
selection [x] = [x]
selection lista = (menor lista) : (selection (remove (menor lista) lista))